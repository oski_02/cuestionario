@ECHO OFF

rem cls

rem Store previous working directory:
set PREV_DIR=%cd%

rem find where this command is being called from
set LOCAL_PATH=%~dp0
rem echo LOCAL_PATH is %LOCAL_PATH%

rem SERVER_STARTUP="$LOCAL_PATH/core/node $LOCAL_PATH/server/server.js"
set SERVER_STARTUP=%LOCAL_PATH%\core\node64.exe %LOCAL_PATH%\server\server.js

echo Starting server with command:
echo %SERVER_STARTUP%

%SERVER_STARTUP%

rem restore previous working dir
cd %PREV_DIR%

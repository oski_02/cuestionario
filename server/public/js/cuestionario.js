var i = 0;
var aciertos = 0;
var fallos = 0;
var preguntas;

var xhttp = new XMLHttpRequest();
var url = "questionList";
var list;
var i=0;

var aciertosNecesarios=5; // Esto debería venir del servidor y las estrellas generarse dinámicamente
var currentQ;
var level;

var tiempo_inicio=0;
var tiempo_fin=0;
var tiempo_total=0;

if (level=='12') {
    
}

// Once GET request for question list is ready:
xhttp.onreadystatechange = function() {

	if (xhttp.readyState == 4 && xhttp.status == 200) {
		list = JSON.parse(xhttp.responseText);
		loadQuestion(i);
        
	}
};
xhttp.open("GET", url, true);
xhttp.send();

$('.overlay').show();
$('#bienvenido').show();

function comenzar(){
    // Leer el número de estrellas seleccionado por el usuario
    aciertosNecesarios=$('#selec_nivel').val();
    console.log("aciertosNecesarios: "+aciertosNecesarios);
    pintaEstrellas();
    tiempo_inicio=new Date().getTime();
    console.log("Hora de inicio: "+tiempo_inicio);
    $('.overlay').hide();
    $('#bienvenido').hide();


}

function pintaEstrellas(){
    var estrella;
    // Y pinta el numero de estrellas
    for (s=1; s<=aciertosNecesarios; s++) {
        starNo="starN"+s;
        estrella='<div class="box_star"><img class="img_star" id="'+starNo+'" src="pics/noStar.jpeg" alt="'+starNo+'"/></div>';
        //$(estrella).find('img').attr('id',starNo);
        $('#stars').append(estrella);
    }
}

function loadQuestion (qindex) {
    currentQ=list.preguntas[qindex];
    
    // Si la pregunta es de tipo "qwpic" cargar y mostrar imagen
    if (currentQ.qt=='qwpic'){
        $('#qpic').attr('src', currentQ.src);
        // Pista para los peques
        $('#qpic').attr('title', currentQ.a);
        $('#qpic').show();
    }
    else {
        $('#qpic').hide();
    }
    // Cargar enunciado de la pregunta
    //document.getElementById("cues").innerHTML = list.preguntas[qindex].q;
    // Same with jQuery
    $('#cues').text(currentQ.q);
    
}

function check(){
//esta funcion comprueba la respuesta
	response = document.getElementById("res").value.toLowerCase();	
	if (response == list.preguntas[i].a.toLowerCase()){
		// Respuesta correcta
        aciertos++;
		if (aciertos > aciertosNecesarios){return;}
		
		if (aciertos <aciertosNecesarios){
			i++;
			document.getElementById("starN" + aciertos).src = "pics/yesStar.jpeg";
			loadQuestion(i);
			document.getElementById("res").value = "";
			//alert("Respuesta correcta!!!!");
		}
		if (aciertos == aciertosNecesarios){
            console.log("Ya puedes jugar");
            // Llenar la última estrella
            document.getElementById("starN" + aciertos).src = "pics/yesStar.jpeg";
            
            tiempo_fin=new Date().getTime();
            console.log("Hora de final: "+tiempo_fin);
            
            tiempo_total=convertirms(tiempo_fin-tiempo_inicio);
            console.log("Tiempo total: "+tiempo_total);
            $('#tiempo').text(tiempo_total);
            $('#totalfallos').text(fallos);
            
            $('.overlay').show();
            $('#enhorabuena').show();
            
            //$('#cues').text("¡¡Enhorabuena!!\nAhora ya puedes jugar :)");
            //$('#res').hide();
            //alert("¡¡Enhorabuena!!\nAhora ya puedes jugar :)");
            // Display pop-up
            
        }
	} 
    else {// Respuesta incorrecta
		
        fallos++;
        // Actualizar el contador de fallos
        $('#cont_fallos').text(fallos);
        //alert("¡Respuesta incorrecta!\nLa respuesta era: " +list.preguntas[i].a.toLowerCase());
        // Actualizar la respuesta correcta en el popup
        $('#res_correcta').text(list.preguntas[i].a);
        // Y mostrar el popup
        $('.overlay').show();
        $('#incorrecta').show();
		if (aciertos <aciertosNecesarios){
			i++;
			loadQuestion(i);
		}
		document.getElementById("res").value = "";
	}
}

// Esta es la función que faltaría por definir; estaba puesta en el html para cuando se presiona una tecla. Toma como argumento un evento

function keypressed (e) {
    console.log("Some key was pressed; event keyCode: %s",e.keyCode);
    if(e.keyCode == 13) {
        e.preventDefault();
        console.log("Enter key detected; calling check()");
        check();
        return false; // Así se evita que el evento por defecto de la tecla intro se lance - intenta mandar el formulario submit y recarga la página entera
    }
}

//$(document).ready(function(){

$('.overlay').on('click',function(){
    console.log($(this).attr('id')+" clicked!");
    $('.overlay').hide();
    $('.modal').hide();
});
    
//});

// Funciones

/**
 * Convierte un número de milisegundos a minutos y segundos
 *
 * @param integer milisegundos tiempo en milisegundos a convertir
 * @return String Tiempo en minuto(s) y segundos
 */
function convertirms (milisegundos){
    console.log("Convirtiendo %s milisegundos", milisegundos);
    //Paso de milisegundos a segundos
    var segundos = (Math.floor(milisegundos/1000))%60;
    console.log("segundos: %s", segundos);
    var respuesta=segundos+" segundos";
    
    var minutos = Math.floor(milisegundos/1000/60);
    console.log("minutos: %s", minutos);
    if (minutos > 0) {
        // Si es más de un minuto poner "minutos"
        if (minutos > 1) {
            respuesta=minutos+" minutos y "+respuesta;
        } 
        else {
            respuesta=minutos+" minuto y "+respuesta;
        }
    }
    
    return respuesta;
}


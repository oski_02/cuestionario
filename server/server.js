var http = require ("http");
var fs = require("fs");
var url = require("url");
var express = require("express");
var path = require('path');

// Parámetros
var port = 8082;
var batchSize=20; //Número de preguntas en una batería

var allQuestions=[];
var totalQuestions;

// Creamos la instancia del servidor y la carpeta cuyos archivos vamos a servir (public)
var server=express();
server.use(express.static(path.join(__dirname, 'public')));

// Funciones del servidor
// loadQuestions - se ejecuta al arranque del servidor
function loadQuestions () {
    
    var filename=path.join(__dirname,'files/listaCuestiones.json');
   
	//Read the requested file content from file system
	allQuestions = JSON.parse(fs.readFileSync(filename,'utf8')).preguntas;
    totalQuestions=allQuestions.length;
    console.log("%s questions loaded succcessfully!", totalQuestions);
}

// Fisher-Yates shuffle
function shuffle(array) {
  var m = array.length, t, i;

  // While there remain elements to shuffle…
  while (m) {

    // Pick a remaining element…
    i = Math.floor(Math.random() * m--);

    // And swap it with the current element.
    t = array[m];
    array[m] = array[i];
    array[i] = t;
  }

  return array;
}

// Devuelve un objeto JSON con 10 preguntas aleatorias
// Usa un array auxiliar para no repetirse
function getBatch() {
    
    var batch=[]; // Return array
    var m=totalQuestions;
    var auxArray=allQuestions.slice(0); // Clona el array allQuestions
    var r;
    console.log("Batch size is %s", batchSize);
    
    for (var i=0; i<batchSize; i++) {
        // Número aleatorio de 0 a la longitud del array auxiliar con preguntas restantes
        r = Math.floor(Math.random() * m--);
        console.log("Adding question %s in position %s", r, i);
        batch[i]=auxArray.splice(r, 1)[0];
    }
    
    console.log("Batch size is %s", batchSize);
    console.log("m is %s", m);
    // Insert array into a JSON object
    batch={preguntas:batch};
    //console.log("Returning array %s", JSON.stringify(batch));
    return batch;
    
}

// Definición de los servicios



// Punto de entrada - servir la página principal (cuestionario.html)
server.get('/', function(request,response){
    console.log("\nServing home page to ip: %s", request.ip);
    var page=fs.readFileSync(path.join(__dirname, 'public/cuestionario.html'), 'utf8');
    response.send(page);
    response.end();
});

// Devuelve un array json con una batería aleatoria de preguntas
server.get('/questionList', function(req,res){
    console.log("Request: %s from %s; url: %s",req.method,req.ip, req.url);
        var newBatch=getBatch();
        //console.log("Sending batch with %s questions:\n %s", batchSize, JSON.stringify(newBatch));
        res.send(newBatch);
        /*
        if (err){
            console.log(err);
            //HTTP Status: 404 : NOT FOUND;
            //Content Type: text/plain
            res.writeHead(404, {"Content-Type": "text/html"});
        }else{
            //Page found	  
            // HTTP Status: 200 : OK
            // Content Type: text/plain
            res.writeHead(200, {"Content-Type": "text/html"});

            // Write the content of the file to response body
            res.write(data.toString());	
        }
        */
        //send the response body
        
});

// Aquí arrancamos el servidor
server.listen(port, function () {
    // Y mostramos el mensaje una vez que ya está corriendo
    console.log('Server running at localhost:'+port+'/');
    
});
// Y cargamos la batería de preguntas
loadQuestions();
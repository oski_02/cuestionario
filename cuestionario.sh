#!/bin/bash

clear

# Store previous working directory:
PREV_DIR=`pwd`
# echo "Prev dir is $PREV_DIR"

# find where this command is being called from
LOCAL_PATH=${0%/*}
echo LOCAL_PATH is $LOCAL_PATH


# No funciona al traer el binario desde el repositorio - hay que buscar alternativa (instalar node?)
SERVER_STARTUP="$LOCAL_PATH/core/node $LOCAL_PATH/server/server.js"

echo "Starting server with command:"
echo $SERVER_STARTUP 
#$SERVER_STARTUP &> $SERVER_LOGFILE &
$SERVER_STARTUP
echo "Server result: $?"
if [ $? -ne "0" ] ; then
    echo "Server failed to start, exiting now"
    cd $PREV_DIR
    exit;
fi


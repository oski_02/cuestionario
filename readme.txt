
Para utilizar el cuestionario, bajar el repositorio de bitbucket, abrir un terminal, ejecutar el script "cuestionario.sh" (o abrirlo como script con doble clic).

A continuación, acceder usando la siguiente url:
http://localhost:8082/

Para acceder desde otra máquina necesitamos apuntar a la IP del servidor, por ejemplo.
http://192.168.1.169:8082/

En la carpeta core están los binarios de node para linux y windows.

Para arrancar el servidor en Windows habría que crear otro script similar que se llame cuestionario.cmd


El javascript cliente puede identificar el "tipo" de pregunta. Si es del tipo "qwpic" (question with picture), mostrará la imagen indicada en el campo src del json, junto con el enunciado de la pregunta.

Por ejemplo, una pregunta "qwpic" en json sería:
{"qid":"33","qt": "qwpic", "src":"https://upload.wikimedia.org/wikipedia/commons/f/f2/Flag_of_Costa_Rica.svg","q":"¿De qué país es esta bandera?","a":"Costa Rica"},

--- Esta parte debería ir a la wiki ---
Contenido de la base de datos de preguntas.
Cada entrada en la colección de preguntas incluye los siguientes campos:

qid - identificador único de pregunta
q   - enunciado de la pregunta
a   - respuesta esperada
qt  - (Question type) tipo de pregunta

Lado servidor:
Al arrancar el servidor se cargarán las preguntas del archivo JSON a un array

La función /questionList enviará 10 preguntas al azar del conjunto total de preguntas.

--- Y esto a los "issues"/ enhancements
Tareas:
    - Crear una interfaz para añadir más preguntas 
        - Ponerle acceso para "profes"
    
    - Crear el modelo de datos con distintas baterías de preguntas para cada nivel
    
    - Más adelante:
        - crear una pequeña tabla con los nombres de usuario de los alumnos
        - registrar los resultados de cada alumno 
            - usando el identificador qid de cada pregunta
            
        - En cada sesión/batería, cada alumno deberá recibir sobre todo preguntas falladas recientemente, junto con alguna de las que acertó.
            - La proporción debería ser parametrizable
        - Otra

--- A partir de aquí deberíamos borrar... ---

Este es el archivo Readme.txt
Creare el projecto de Arse para el cuestionario de sus alumnos... porque soy un puto fiera

	Arse: voy a intentar crear una rama (no lo he hecho nunca) para trabajar en una tarea concreta, por ejemplo arreglar un bug.
A ver si consigo hacer que la pág reconozca si le das al intro después de escribir la respuesta.

	oski_02: AJAJa me ha llevado un tiempo pero he comprobado que lo de "porque soy un puto fiera" no lo puse yo ajajaja estaba diciendo.."dios que verguenza". 
Te comento: 
>git log --oneline //Esto te muestra el historial de commits en una linea con un codigo y el mensage
>git checkout <codigo_commit> //Esto te situa justo en el estado despues de realizar el commit
asi que he comprobado un par de estados anteriores y he comprobado que yo no lo puse!!! jajaja 
pero Gracias :D:D
Ahora puedes borrar todo esto si quieres, era por si no lo sabias.

	oski_02: El branch estaba correctamente creado y lo del Enter como sabras funciona perfecto jaja, tambien estuve intentando solucionar eso pero no lo conseguí. He hecho el merge y dejado un commit cn el, tambien he eliminado el brachn "intro" que creaste. Poco a poco se ven las ventajas de trabajar asi.
Pd: creo el archivo "issues.txt" para que vayamos poniendo tareas que haya que solucionar en un futuro

	oski_02: Arse al parecer hay una interfaz grafica para el git s llama "gitk"
>sudo apt-get install gitk
Y esta es una guia muuuy sencilla:
http://rogerdudler.github.io/git-guide/index.es.html
	
	oski_02(6-Marzo-2016):Arse, te comento, he creado el "server.js" y
"listaCuestiones.txt", he modificado el "cuestionario.html" y el
"script_xml.js". He conseguido al parecer enviar una peticion desde el lado
cliente y que devuelva el documento el servidor.... echale un ojo. He creado
otra Rama para este cambio, no se si puedes tener acceso a ella..supongo q
si..
